import { readdirSync } from 'node:fs';
import { defineConfig } from "vite";
import { ViteImageOptimizer } from "vite-plugin-image-optimizer";
import { resolve } from 'node:path';
import handlebars from 'vite-plugin-handlebars';
import hbs from 'handlebars';
import { ViteMinifyPlugin } from 'vite-plugin-minify'
import { parse } from "yaml";
import { readFileSync } from "node:fs";

export default defineConfig({
  build: {
    outDir: 'public',
  },
  publicDir: 'assets',
  root: './',
  plugins: [handlebars({
    partialDirectory: resolve(__dirname, 'src/partials'),
    helpers: {
      project: (name) => {
        const project = parse(readFileSync(resolve(__dirname, `src/projects/${name}.yml`), 'utf-8'));
        const template = readFileSync(resolve(__dirname, 'src/templates/project.hbs'), 'utf-8');
        const tpl = hbs.compile(template);
        return tpl(project);
      },
      social: () => {
        const res = [];
        const template = readFileSync(resolve(__dirname, 'src/templates/social.hbs'), 'utf-8');
        const files = readdirSync('src/socials').filter((n) => n.endsWith(".yml")).sort();
        files.forEach((file, _) => {
          const project = parse(readFileSync(resolve(__dirname, `src/socials/${file}`), 'utf-8'));
          const tpl = hbs.compile(template);
          res.push(tpl(project));
        });
        return res.join("");
      },
      talk: (date) => {
        const res = [];
        const template = readFileSync(resolve(__dirname, 'src/templates/talk.hbs'), 'utf-8');
        const files = readdirSync('src/talks').filter((n) => n.endsWith(".yml")).sort((a, b) => b.localeCompare(a));
        files.forEach((file, _) => {
          const talk = parse(readFileSync(resolve(__dirname, `src/talks/${file}`), 'utf-8'));
          const tpl = hbs.compile(template);
          res.push(tpl(talk));
        });
        return res.join("");
      }
    }
  }), ViteImageOptimizer(), ViteMinifyPlugin()],
});

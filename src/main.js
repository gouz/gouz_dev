import "./style.css";
import Flub from "./lib/flub";

if (window.innerWidth > 640) {
  ["socials", "talks", "projects"].forEach((e, _) => new Flub(`#${e}-wrapper`));
  const $footer = document.querySelector("footer");
  [...document.querySelectorAll("li")].forEach((li, _) => {
    li.addEventListener("mouseenter", () => {
      $footer.innerText = li.querySelector("span").innerText;
    });
    li.addEventListener("mouseleave", () => {
      $footer.innerText = "";
    });
  });
}

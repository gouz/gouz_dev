export default class Flub {
  constructor(selector, opts) {
    this.opts = opts != null ? opts : {};
    if (this.opts.min == null) {
      this.opts.min = 0;
    }
    if (this.opts.max == null) {
      this.opts.max = 360;
    }
    if (this.opts.speed == null) {
      this.opts.speed = 400;
    }
    if (this.opts.dispatch == null) {
      this.opts.dispatch = true;
    }
    if (this.opts.sync == null) {
      this.opts.sync = false;
    }
    if (this.opts.elastic == null) {
      this.opts.elastic = true;
    }
    if (this.opts.blur == null) {
      this.opts.blur = true;
    }
    const flubber = document.querySelector(selector);
    flubber.style.position = "relative";
    this._launcher = flubber.querySelector(
      flubber.getAttribute("data-launcher")
    );
    this._wrapper = document.querySelector(this._launcher.getAttribute("href"));
    let rect = this._launcher.getBoundingClientRect();
    this._button = {
      top: rect.top + document.body.scrollTop,
      left: rect.left + document.body.scrollLeft,
      radius: rect.height / 2,
    };
    this._launcher.style.zIndex = 2;
    this._launcher.style.position = "absolute";
    this._wrapper.style.position = "absolute";
    this.populate();
    rect = this._items[0].getBoundingClientRect();
    this._radius = rect.width / 2;
    this._cnst = Math.PI / 180;
    this._launcher.addEventListener(
      "click",
      (e) => {
        e.preventDefault();
        if (this._wrapper.classList.contains("open")) {
          this._wrapper.classList.remove("open");
          this._items.forEach((i) => {
            i.style.left = 0;
            i.style.top = 0;
            i.style.opacity = 0;
          });
        } else {
          this.position();
        }
        return false;
      },
      false
    );
    return this;
  }

  position() {
    this._wrapper.classList.add("open");
    let l = this._items.length;
    let D = 2.5 * this._button.radius;
    let N = 0;
    let f = 0;
    let ang = this.opts.max - this.opts.min;
    const calc = () => {
      N = ~~((ang / 60) * (D / (this._radius * 2)));
      if (N > l && this.opts.dispatch) {
        N = l;
      }
      f = ang / N;
    };
    calc();
    let c = 0;
    let m = 0;
    this._items.forEach((i) => {
      if (c == N) {
        c = 0;
        m++;
        D += 2.2 * this._button.radius;
        l -= N;
        calc();
      }
      let a = this._cnst * (this.opts.min + f * c++);
      i.style.left = `${D * Math.cos(a)}px`;
      i.style.top = `${-(D * Math.sin(a))}px`;
      i.style.opacity = 100;
    });
    return this;
  }
  populate() {
    this._items = [];
    Array.from(this._wrapper.children).forEach((i) => {
      if (i.nodeType != 8) {
        this._items.push(i);
        i.style.position = "absolute";
        i.style.top = 0;
        i.style.left = 0;
        i.style.opacity = 0;
      }
    });
    let c = 0;
    Array.from(this._wrapper.children).forEach((i) => {
      i.style.transition = `all ease-out ${this.opts.speed}ms`;
      i.style.transitionDelay = `${(c++ * this.opts.speed) / 10}ms`;
      if (this.opts.elastic) {
        i.style.transitionTimingFunction =
          "cubic-bezier(0.66,-0.07, 0.06, 1.55)";
      }
    });
    return this;
  }
  toggle() {
    const event = document.createElement("HTMLEvents");
    event.initEvent("click", false, false);
    this._launcher.dispatchEvent(event);
    return this;
  }
}
